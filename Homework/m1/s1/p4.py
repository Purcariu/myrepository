"""
p4. Creați o funcție care va determina țările "nevizitate" dintr-un fișier.
Funcția va avea 3 parametri, nume de fișiere.
Primul fișier conține numele unor țări și va fi descărcat
de aici, al doilea va conține numele unor țări "vizitate"
din primul fișier la alegere și al treilea va fi numele
fișierului în care vor fi scrise țările "nevizitate".
"""


def func_4(countries, visited, to_be_visited):
    # Numele tarilor trebuie sa fie unele sub
    # altele ca sa poata fi citite corect
    with open(countries, 'r') as c:

        c_lines = [l.strip('\n') for l in c.readlines()]

    with open(visited, 'r') as v:
        v_lines = [l.strip('\n') for l in v.readlines()]

    with open(to_be_visited, 'w') as tbv:

        for country in c_lines:
            if country not in v_lines:
                tbv.write('{}\n'.format(country))


func_4('countries.txt', 'visited.txt', 'to_be_visited.txt')
