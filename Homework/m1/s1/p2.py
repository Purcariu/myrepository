"""
p2. Creați o funcție care va primi ca parametri numele unui
fișier text și un șir de caractere.
Funcția va fi apelată cu argumentele din linia de comandă.
Aceasta va citi textul linie cu linie, liniile care conțin șirul de caractere
vor fi stocate într-o listă care va fi returnată.

Input:
python p2.py my_file.txt Ana are mere
func_2('my_file.txt', 'Ana are mere')

Output:
['Cristi știa că Ana are mere', 'Ana are mere dar și pere']
"""
import sys.argv


def func_2(txt, char):
    array = []
    with open('{}'.format(txt), 'r') as f:
        for l in f.readlines():
            if char in l:
                array.append(l)

    return array


print(func_2(sys.argv[1], sys.argv[2]))
