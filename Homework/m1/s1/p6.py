'''
p6. Creați o funcție care returnează o funcție(lambda) de comparare.
Funcția primește un parametru care poate fi numeric sau string.
Funcțiile vor compara numere după valoare,
respectiv șiruri de caractere după lungime.
Funcțiile vor returna -1, 0 sau 1 pentru a<b, a=b, respectiv a>b.

Input:
cmp1 = func_6(0)
print(cmp1(2, 3))
cmp2 = func_6('')
print(cmp2('ana are', 'adi are'))
Output:
-1
0
'''


def func_6(p):

    if type(p) == str:
        return lambda a, b: -1 if len(a) < len(b) \
            else 0 if len(a) == len(b) else 1
    else:
        return lambda a, b: -1 if a < b else 0 if a == b else 1


cmp1 = func_6(0)
print(cmp1(2, 3))
cmp2 = func_6('')
print(cmp2('ana are', 'adi are'))
