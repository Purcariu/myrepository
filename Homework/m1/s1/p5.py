"""
p5. Creați o funcție care simulează comportamentul funcției map.
Aceasta primește ca parametri o funcție f și un iterabil
(list, dict, str, tuple etc) și va returna un iterabil de același tip.

Input:
func_5(squared, (1, 1, 2, 3, 5, 8))
Output:
(1, 1, 4, 9, 25, 64)
"""


def func_5(f, iterable):
    result = []
    for i in iterable:
        result.append(f(i))

    return result


def squared(l):

    return l * l


print(func_5(squared, (1, 1, 2, 3, 5, 8)))
