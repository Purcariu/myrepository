"""
p1. Creați o funcție care determină și returnează valoarea maximă și valoarea
minimă dintre un șir de numere primite ca parametrii în *args.
Funcția va returna o tuplă (min, max).
Input:
func_1(73, 21, 7, 321, 5, 8)
Output:
(5, 321)
"""


def func_1a(*args):

    a = min(args)
    b = max(args)

    return (a, b)


def func_1b(*args):
    a = args[0]
    b = 0
    for i in args:
        if i > b:
            b = i
        if i < a:
            a = i

    return (a, b)
