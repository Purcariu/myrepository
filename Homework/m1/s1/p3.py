"""
p3. Creați o funcție care va primi ca parametri 2 liste, una care va conține
numele unor persoane respectiv o listă ce va
conține anul nașterii pentru fiecare persoană.
Funcția va folosi listele primite pentru a
crea o listă de tuple de tip (nume, an_nastere).
Lista va fi sortată crescător după anul nașterii fiecărei persoane, convertită
la dicționar(dict) și returnată de către funcție.

Input:
func_3(['Gabi', 'Andreea', 'Paul'], [1993, 1989, 1997])
Output:
{'Andreea': 1989, 'Gabi': 1993, 'Paul': 1997}
"""


def func_3(a, b):
    tuple_list = []

    for nume, an in zip(a, b):
        tuple_list.append((nume, an))

    for l in range(1, len(tuple_list)):
        if tuple_list[l][1] < tuple_list[l - 1][1]:
            tuple_list.insert(0, tuple_list[l])
            del tuple_list[l + 1]

    tuple_list = dict(tuple_list)

    return(tuple_list)


print(func_3(['Gabi', 'Andreea', 'Paul'], [1993, 1989, 1997]))
