# De preferat ca programul sa fie deschis folosind cmd
# pentru a afisa meniurile corespunzator.

import collections
import os
import time

contacts = collections.defaultdict(str)


def show_agenda():
    if len(list(contacts.keys())) == 0:
            print('You have no contact in agenda.\n')
    for c in list(contacts.keys()):
        print('{} : {}\n'.format(c, contacts.get(c)))
    input('Press enter to go to main menu: ')


def add_contact():
    number_list = []
    contact = input('Enter a contact: ')
    if contact in list(contacts.keys()):
        print('\nContact already exist!\n')
        input('Press enter to go to main menu: ')
    else:
        number = input('Enter a number: ')
        number_list.append(number)
        contacts[contact] = number_list


def add_nr_to_contact():
    contact = input('Enter a contact: ')
    if contact in contacts.keys():
        number = input('Enter a number: ')
        if number in contacts.get(contact):
            print('\nThe contact have this number yet!')
            time.sleep(2)
        else:
            contacts[contact].append(number)
    else:
        print("\nThe contact doesn't exist.\n")
        input('Press enter to go to main menu: ')


def find_contact():
    contact = input('Enter contact: ')
    if contact in contacts.keys():
        print('\n{} : {}\n'.format(contact, contacts.get(contact)))
    else:
        print("\nContact doesn't exist.\n")
    input('Press enter to go to main menu: ')


def delete_contact():
    contact = input('Enter contact: ')
    if contact not in contacts.keys():
        print("\nContact doesn't exist!.\n")
        input('Press enter to go to main menu: ')
    else:
        contacts.pop(contact)


def main_menu():
    print('*' * 15 + ' MENU ' + '*' * 15)
    print('1 - Show agenda')
    print('2 - Add contact')
    print('3 - Add phone nr. to contact')
    print('4 - Find contact')
    print('5 - Delete contact')
    print('0 - Exit')
    print('*' * 36)


while True:
    os.system('cls')
    main_menu()
    try:
        option = int(input('Your option: '))
        if option not in range(6):
            print('Input must be a number between 0 and 5!\n')
            time.sleep(2)
            continue
    except ValueError:
        print('Input must be a number!')
        time.sleep(2)
        continue
    if option == 1:
        os.system('cls')
        show_agenda()
        os.system('cls')
    elif option == 2:
        os.system('cls')
        add_contact()
        os.system('cls')
    elif option == 3:
        os.system('cls')
        add_nr_to_contact()
        os.system('cls')
    elif option == 4:
        os.system('cls')
        find_contact()
        os.system('cls')
    elif option == 5:
        os.system('cls')
        delete_contact()
        os.system('cls')
    elif option == 0:
        os.system('cls')
        print('Have a nice day!')
        time.sleep(1)
        os.system('cls')
        exit()
